// Copyright (C) 2023 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

filegroup {
    name: "sdksandbox-service-proto",
    srcs: [
        "*.proto",
    ],
}

genrule_defaults {
    name: "gen_platform_api_allowlist_per_target_sdk_version",
    tools: ["aprotoc"],
    tool_files: [ "Verifier.proto" ],
    cmd: "$(location aprotoc) --encode=com.android.server.sdksandbox.proto.AllowedApisPerTargetSdk " +
        "-I $$(dirname $(location Verifier.proto))" +
        " $(location Verifier.proto) < $(in) > $(out)",
    visibility: [
        "//packages/modules/AdServices/sdksandbox/service"
    ],
}

genrule {
    name: "platform_api_allowlist_per_target_sdk_version_current.binarypb",
    defaults: ["gen_platform_api_allowlist_per_target_sdk_version"],
    srcs: [ "platform_api_allowlist_per_target_sdk_version_current.textproto"],
    out: ["platform_api_allowlist_per_target_sdk_version_current.binarypb"],
}

// Generate the Proto builders, etc.
java_library {
    name: "sdksandbox-service-proto-lite",
    sdk_version: "core_current",
    proto: {
        type: "lite",
        include_dirs: [
            "external/protobuf/src",
            "external/protobuf/java"
        ],
    },
    srcs: [
        ":sdksandbox-service-proto",
        ":libprotobuf-internal-protos",
    ],
    static_libs: ["libprotobuf-java-lite"],
    apex_available: ["com.android.adservices"],
}
