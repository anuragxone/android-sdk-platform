/*
 * Copyright (C) 2023 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.adservices.service.adselection;

import static com.android.adservices.service.adselection.AdSelectionScriptEngine.NUM_BITS_STOCHASTIC_ROUNDING;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class AdCostTest {
    private static final AdCost AD_COST = new AdCost(1.0, NUM_BITS_STOCHASTIC_ROUNDING);

    @Test
    public void testAdCostEquals() {
        assertEquals(AD_COST, AD_COST);
    }

    @Test
    public void testAdCostNotEquals() {
        assertNotEquals(AD_COST, new AdCost(2.0, NUM_BITS_STOCHASTIC_ROUNDING));
    }
}
