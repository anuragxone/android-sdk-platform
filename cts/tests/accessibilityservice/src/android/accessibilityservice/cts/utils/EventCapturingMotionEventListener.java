/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.accessibilityservice.cts.utils;

import static android.view.MotionEvent.ACTION_HOVER_MOVE;
import static android.view.MotionEvent.ACTION_MOVE;

import static com.google.common.truth.Truth.assertThat;
import static com.google.common.truth.Truth.assertWithMessage;

import static java.util.concurrent.TimeUnit.SECONDS;

import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class EventCapturingMotionEventListener
        implements View.OnTouchListener, View.OnHoverListener {
    private static final long WAIT_TIME_SECONDS = 5;
    private static final long MIN_WAIT_TIME_SECONDS = 2;
    // whether or not to keep events from propagating to other listeners
    private boolean mShouldConsumeEvents;
    private final BlockingQueue<MotionEvent> mEvents = new LinkedBlockingQueue<>();

    public EventCapturingMotionEventListener(boolean shouldConsumeEvents) {
        this.mShouldConsumeEvents = shouldConsumeEvents;
    }

    public EventCapturingMotionEventListener() {
        this.mShouldConsumeEvents = true;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return onMotionEvent(motionEvent);
    }

    @Override
    public boolean onHover(View view, MotionEvent motionEvent) {
        return onMotionEvent(motionEvent);
    }

    private boolean onMotionEvent(MotionEvent event) {
        assertThat(mEvents.offer(MotionEvent.obtain(event))).isTrue();
        return mShouldConsumeEvents;
    }

    /** Insure that no motion events have been detected. */
    public void assertNonePropagated() {
        try {
            MotionEvent event = mEvents.poll(MIN_WAIT_TIME_SECONDS, SECONDS);
            assertThat(event).isNull();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Check for the specified motion events. Note that specifying ACTION_MOVE or ACTION_HOVER_MOVE
     * will match one or more consecutive events with the specified action because it is impossible
     * to tell in advance how many move events will be generated by a gesture.
     */
    public void assertPropagated(int... eventTypes) {
        MotionEvent ev;
        try {
            List<String> expected = new ArrayList<>();
            List<String> received = new ArrayList<>();
            for (int e : eventTypes) {
                expected.add(MotionEvent.actionToString(e));
            }
            ev = mEvents.poll(WAIT_TIME_SECONDS, SECONDS);
            assertWithMessage(
                            "Expected "
                                    + expected
                                    + " but none present after "
                                    + WAIT_TIME_SECONDS
                                    + " seconds")
                    .that(ev)
                    .isNotNull();
            // By this point there is at least one received event.
            received.add(MotionEvent.actionToString(ev.getActionMasked()));
            ev = mEvents.poll(WAIT_TIME_SECONDS, SECONDS);
            while (ev != null) {
                int action = ev.getActionMasked();
                String current = MotionEvent.actionToString(action);
                String prev = received.get(received.size() - 1);
                if (action != ACTION_MOVE && action != ACTION_HOVER_MOVE) {
                    // Add the current event if the previous received event was not ACTION_MOVE or
                    // ACTION_HOVER_MOVE
                    // There is no way to predict how many move events will be generated by a
                    // gesture
                    // so specifying a move event matches one or more.
                    received.add(MotionEvent.actionToString(action));
                } else {
                    // The previous event was a move event. Ignore subsequent events with the same
                    // action.
                    if (!prev.equals(current)) {
                        received.add(current);
                    }
                }
                if (expected.size() == received.size()) {
                    // Avoid waiting an extra set of seconds just to confirm we are at the end of
                    // the stream.
                    break;
                }
                ev = mEvents.poll(WAIT_TIME_SECONDS, SECONDS);
            }
            assertThat(received).isEqualTo(expected);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public List<MotionEvent> getRawEvents() {
        List<MotionEvent> motionEvents = new ArrayList<>();
        MotionEvent ev;
        try {
            ev = mEvents.poll(WAIT_TIME_SECONDS, SECONDS);
            while (ev != null) {
                motionEvents.add(ev);
                ev = mEvents.poll(WAIT_TIME_SECONDS, SECONDS);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        assertThat(motionEvents).isNotEmpty();
        return motionEvents;
    }

    public MotionEvent peek() {
        return mEvents.peek();
    }
}
