# CtsMediaCUJSmallTest

CtsMediaCUJSmallTest is part for cts mediacujtest which does E2E CUJ testing. This test suites uses media3 exoplayer as a player.
CtsMediaCUJSmallTest currently test playback for 15 sec video clips and verifies its playback time.
Input clips are present inside mediacujtest\smalltest\res\raw folder.


## Files for CtsMediaCUJSmallTest

### ForBiggerBlazes details
--------------------------------------------------------------

The original clip is taken from http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4 which is copyrighted to Google LLC.

The clip is downloaded from above link and resampled according to the test requirements using ffmpeg (ffmpeg.org).


### ForBiggerEscapes details
--------------------------------------------------------------

The original clip is taken from http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4 which is copyrighted to Google LLC.

The clip is downloaded from above link and resampled according to the test requirements using ffmpeg (ffmpeg.org).


### ForBiggerJoyrides details
--------------------------------------------------------------

The original clip is taken from http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4 which is copyrighted to Google LLC.

The clip is downloaded from above link and resampled according to the test requirements using ffmpeg (ffmpeg.org).

### ForBiggerMeltdowns details
--------------------------------------------------------------

The original clip is taken from http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4 which is copyrighted to Google LLC.

The clip is downloaded from above link and resampled according to the test requirements using ffmpeg (ffmpeg.org).
