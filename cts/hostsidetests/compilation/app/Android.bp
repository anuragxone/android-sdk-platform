// Copyright (C) 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

java_defaults {
    name: "CtsCompilationApp_defaults",
    defaults: ["cts_support_defaults"],
    srcs: ["src/**/*.java"],
    sdk_version: "29",
    // tag this module as a cts test artifact
    test_suites: [
        "cts",
        "general-tests",
    ],
}

android_test_helper_app {
    name: "CtsCompilationApp",
    defaults: ["CtsCompilationApp_defaults"],
}

android_test_helper_app {
    name: "CtsCompilationApp_with_good_profile",
    defaults: ["CtsCompilationApp_defaults"],
    assets: [":CtsCompilationApp_assets_good_profile"],
}

android_test_helper_app {
    name: "CtsCompilationApp_with_bad_profile",
    defaults: ["CtsCompilationApp_defaults"],
    assets: [":CtsCompilationApp_assets_bad_profile"],
}

genrule {
    name: "CtsCompilationApp_assets_good_profile",
    cmd: "cp $(in) $(out)",
    srcs: [":CtsCompilationApp_profile"],
    out: ["art-profile/baseline.prof"],
}

genrule {
    name: "CtsCompilationApp_assets_bad_profile",
    cmd: "cp $(in) $(out)",
    srcs: [":AppUsedByOtherApp_1_prof"],
    out: ["art-profile/baseline.prof"],
}
